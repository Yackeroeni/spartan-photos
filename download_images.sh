#!/bin/bash

urls=`cat $1.html | sed '/image.photocreate.jp/!d' | sed 's/^.*data-image="\(.*\)" alt="">/\1/'`

mkdir -p "photos/$1"
pushd "photos/$1"

i=0
for url in $urls; do
    let i=$i+1
    filename=`printf %03d.jpg $i`
    curl $url -o $filename
done

popd
