#!/usr/bin/env python

from selenium import webdriver

import sys
import os

idx = sys.argv[1]

driver = webdriver.Chrome()
driver.get('https://g.allsports.jp/en/event/656092');

bibfield=driver.find_element_by_id('zekken_tag_search_zekken')
bibfield.send_keys(idx)

submitbutton=driver.find_element_by_xpath("//button[@type='submit']")
submitbutton.click()

html = driver.page_source
f = open(idx + ".html", "wt")
f.write(html)
f.close()

driver.close()

os.system('./download_images.sh ' + idx)
os.system('./sort.sh ' + idx)
