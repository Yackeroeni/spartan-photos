#!/bin/bash

pushd "photos/$1"

for file in `ls .`; do
    res=`file $file | sed 's/^.*, \([0-9]*x[0-9]*\),.*$/\1/'`
    mkdir -p $res
    mv "$file" "$res/$file"
done

popd
